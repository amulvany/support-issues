---
layout: markdown_page
title: "Offer Packages"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Offers

Before an offer is created, ensure the approval should go through [Greenhouse or BambooHR](/handbook/people-operations/promotions-transfers/#offer-process-in-bamboohr-or-greenhouse).

### Offer Package in Greenhouse

#### Justification Section

This step is optional per each function.

Once it is determined that a candidate will be moving to the offer stage, the hiring manager will answer the following questions in the justification stage in the candidates greenhouse profile:
- In what specific way(s) does this candidate make the team better?
- What flags were raised during the interview process?
- How do we intend on setting this candidate for success?

### Offer Package Creation

The hiring manager will work with the recruiter on the offer details and the recruiter will be responsible for submitting the official offer details through Greenhouse.

To create the offer package, move the candidate to the "Offer" stage in Greenhouse and select "Manage Offer." Input all required and relevant information, ensuring its correctness, and submit; then click `Request Approval`. **Please note that any changes in compensation packages will result in needing re-approval from each approver.**

Note that the offer package should include the candidate's proposed compensation in the most appropriate currency and format for their country of residence and job role. Annual and monthly salaries should be rounded up or down to the nearest whole currency unit and should always end with a zero (e.g., "50,110.00" or "23,500.00"). Hourly rates should be rounded to the nearest quarter-currency unit (e.g., 11.25/hr.).

For internal hires, be sure to include in the "Approval Notes" section the candidate's current level and position, as well as their compensation package.

You can also include any mitigating circumstances or other important details in the "Approval Notes" section of the offer details. If the comp has a variable component, please list base, on target earnings (OTE), and split in the "Approval Notes."

Please make sure that the level and position match the role page.

In case it is a public sector job family, please note (the lack of) clearances.

Information in the offer package for counter offers should include the following in the "Approval Notes" section:

   - New offer:
   - Original offer:
   - Candidate's salary expectation beginning of process:
   - Candidate's counter offer:

Anyone making comments regarding an offer should make sure to @mention the recruiter and hiring manager.

1. The People Ops Analyst will receive an email notifying them of the offer.
   * The People Ops Analyst will ensure the compensation is in line with our compensation benchmarks.
   * Only one approval is needed in order to move forward.
   * If the hire is not in a low location factor area above 0.9, the e-group member responsible for the function and the CFO will be notified.
1. Next, The People Business Partners will receive a notification to approve.
1. Next, the executive of the division will then receive a notification to approve.
1. Lastly, for manager, [staff engineer](/job-families/engineering/developer/#staff-developer), [principal product manager](/job-families/product/product-manager/index.html.md#principal-product-manager), and above roles the CEO and Chief Culture Officer will receive a notification to approve
   * Only one approval is required in order to move forward with the offer.
   * Typically, the Chief Culture Officer will provide the final approval, but if the CCO is out of office, the CEO will be the final approver.


It is recommended to also ping approvers, especially the executive (and CEO if needed) in Slack with the message "Hiring approval needed for [Candidate Name] for [Position]" with a link to the candidate profile. To create the link, search for the candidate in Greenhouse, select the candidate, go to their offer details page, and copy the link. **Do not copy a link from a different section of their candidate profile.**

### Final offer approval

For pending offer approvals needed from the CPO/CEO, there is an `#offers` Slack channel where the requests should be added. This is especially relevant if the CPO is out of office and the CEO is approving offers; the CEO should always be @mentioned for their approval. This Slack channel is private and only the recruiting team, CPO, CEO, and CFO have access to it. Please ensure your ping has:

1. Name
1. Position
1. For re-approvals clearly indicate what changed and why.

The CPO and CEO appreciate the thank you messages but they also have a hard time keeping up with slack notifications. There is no need to say thanks, but if you do please add an emoji instead of sending a message.

If the role is for an individual contributor, the CPO or CEO do not need to approve.  However, please post in the '#offers' channel with "Offer has been extended for [Candidate Name] for [Position]" and a link to the candidates Greenhouse profile.

### Communicating the Offer

Once the offer package has been approved by the approval chain, the verbal offer will be given, which will be followed by an official contract, which is sent through Greenhouse.

Offers made to new team members should be documented in Greenhouse through the notes thread between the person authorized to make the offer and the Candidate Experience Specialist.
   -  Greenhouse offer details should be updated as necessary to reflect any changes including start date. Sections updated that will trigger re-approval are noted in Greenhouse.
